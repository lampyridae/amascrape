# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class SpiderItem(scrapy.Item):
    product_id  = scrapy.Field(serializer=str)
    category    = scrapy.Field()
    pass


class AmazonReviewItem(scrapy.Item):
    meta            = scrapy.Field()
    text            = scrapy.Field()
    rating          = scrapy.Field()
    helpful_vote    = scrapy.Field()
    meta_info       = scrapy.Field()
    user_id         = scrapy.Field()
    pass