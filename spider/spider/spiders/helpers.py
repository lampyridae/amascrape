import copy
import re




def check_url(url : str) -> str:

    amazon_url      = 'http://www.amazon.com'
    amazon_url_reg  = '(http).*(amazon).*/'

    if re.match(amazon_url_reg, url):
        return url 
    else:
        return str(amazon_url + url)



def rm_spaces(wrd : str) -> str:
    return re.search('(\w).*', wrd).group()



def add_cat_to_meta(meta : dict, titles : list) -> dict :

    if not 'catgry' in meta:
        meta['catgry'] = list()

    meta_loc = copy.deepcopy(meta)
    for title in titles:
        meta_loc['catgry'].append(title)
        meta_loc['catgry'] = sort_cat_list(meta_loc['catgry'])
    return meta_loc



def sort_cat_list(unsorted_cat_list : str) -> list:
    
    sorted_cat_list = list()

    useless_cats = ['See all']

    for e in unsorted_cat_list:
        if not e in sorted_cat_list and not e in useless_cats:
            sorted_cat_list.append(e)
    return sorted_cat_list
