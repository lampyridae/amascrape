import copy
import logging
import re
import scrapy

from scrapy import log
from spider.items import AmazonReviewItem, SpiderItem
from scrapy import log

from .helpers import check_url, rm_spaces, add_cat_to_meta




class Left_Nav_Spider(scrapy.Spider):
    name = "mainspider"
    amazon_domain = "https://www.amazon.com"
    visited_websites = set()


    def start_requests(self):
        allowed_domains = [
            self.amazon_domain,
        ]

        # for test purposes
        urls = [
            'https://www.amazon.com/gp/site-directory',
            #'https://www.amazon.com/Household-Broom-Heads-Handles/b?ie=UTF8&node=2245511011'                            #leftNav with empty intended box
            #'https://www.amazon.com/Carports/b?ie=UTF8&node=13638750011'                                                #leftNav without intended box
            #'https://www.amazon.com/International-Shipping-Direct/b?ie=UTF8&node=230659011',                            # left_nav page 
            #'https://www.amazon.com/gp/search/other?ie=UTF8&page=1&pickerToList=lbr_brands_browse-bin&rh=n%3A15874201'  # gpsearch page
            #'http://www.amazon.com/Memory-Card-Adapters-Transcend-Accessories/s?rh=n%3A15874201%2Cp_89%3ATranscend',   # department, multiple pages
            #'https://www.amazon.com/Memory-Card-Adapters-tekit-Accessories/s?rh=n%3A15874201%2Cp_89%3Atekit'           # department, single page

        ]

        yield scrapy.Request(url='https://www.amazon.com/gp/site-directory', callback=self.get_cat_links)




    def get_cat_links(self, response):
        # xPaths
        x_path_base = '//div[@class="a-container fsdContainer fsdFullWidthImage"]//a/@href'

        links = list(set(response.selector.xpath(x_path_base)))
        
        for e in links:

            # get only valid urls
            if not re.match('(http)', e.get()):
                
                # create url 
                url_local = check_url(e.get())
                yield scrapy.Request(url=url_local, callback=self.main_crawl)




    def main_crawl(self, response):

        """
        Main crawler. Crawls pages with 'left_nav', 'leftNav' and 'department' boxes.
        """

        log.msg('Main crawl on %s' % response.url, level=log.INFO)




        ########################################################################################
        # Scrape left_nav elements

        left_nav_reg = '//div[@class="left_nav browseBox"]'
        left_nav = response.selector.xpath(left_nav_reg)


        if bool(left_nav):

            log.msg('Found "left_nav" element', level=log.INFO)
            links_in_container = response.selector.xpath('//div[@class="left_nav browseBox"]//a')
            
            # using a list under the assumption that we are dealing with no duplicates
            

            log.msg('Found %s links in "left_nav" element' % str(len(links_in_container)) )
            for e in links_in_container:

                # call in next crawl
                link  = str( check_url(e.xpath('@href').get()) ) 

                # pass as category
                link_title = e.xpath('text()').get()
                meta_loc = add_cat_to_meta(response.meta, [link_title] )
                
                # call next crawl 
                if not link in self.visited_websites:
                    self.visited_websites.add(link)
                    yield scrapy.Request(url=link, callback=self.main_crawl, meta=meta_loc)
                else:
                    pass




        ########################################################################################
        # Site includes 'leftNav' element

        leftNav_xpath                   = '//div[@id="leftNav"]'
        expander_container_xpath        = '//ul[@class="a-unordered-list a-nostyle a-vertical s-ref-indent-two"]'
        expander_container_div_xpath    = '//div[@class="a-row a-expander-container a-expander-extend-container"]'
        

        if bool(response.selector.xpath(leftNav_xpath)):

            log.msg('Found "leftNav" element', level=log.INFO)
            

            # get parent categories
            previous_cats = set()
            previous_cats_selector   = response.selector.xpath( (leftNav_xpath + '//span[@class="srSprite backArrow"]') )
            for e in previous_cats_selector:
                word = e.xpath('../text()').get()
                cleared_word = rm_spaces(word)
                previous_cats.add(cleared_word)


            # get current category
            cat_name        = response.selector.xpath( (leftNav_xpath + '//span//h4/text()') ).get()


            # check for type of box
            # get only first box for each box 

            first_box = None
            if bool(response.selector.xpath(expander_container_xpath)):
                first_box = response.selector.xpath(expander_container_xpath)[0]

            if bool(response.selector.xpath(expander_container_div_xpath)):
                first_box = response.selector.xpath(expander_container_div_xpath)[0]

            if bool(first_box):

                links = first_box.xpath('.//a')
                
                log.msg('Intended link box exist', level=log.INFO)
                
                if len(links) > 0:
                    
                    # more links to follow in the 'a-unordered-list a-nostyle a-vertical s-ref-indent-two'-box
                    for e in links:
                        url_loc  = check_url(e.xpath('@href').get())
                        name     = e.xpath('span/text()').get()

                        previous_cats_list = list(previous_cats)
                        previous_cats_list.append(cat_name)
                        meta = add_cat_to_meta( response.meta, previous_cats_list)
                        if not url_loc in self.visited_websites:
                            self.visited_websites.add(url_loc)
                            yield scrapy.Request(url=url_loc, callback=self.main_crawl, meta=meta)
                        else:
                            pass
                    return
                else:
                    log.msg('Box size is less than 1', level=log.INFO)
                    pass
                
            # no more links to crawl -> search for 'See more' link
            link_reg = '(\/gp\/search\/other)'
            all_links = response.selector.xpath(leftNav_xpath + '//a')
            valid_links = list()
            
            # previous categories 
            previous_cats_list = list(previous_cats)
            previous_cats_list.append(cat_name)
            meta = add_cat_to_meta( response.meta, previous_cats_list)

            # search for /gp/search/other  links
            for e in all_links:
                href = e.xpath('@href').get()
                if re.match(link_reg, href):
                    valid_links.append(check_url(href))

            for e in valid_links:
                # Goto department category
                log.msg('Visit category page: %s' % e, level=log.INFO)
                if not e in self.visited_websites:
                    self.visited_websites.add(e)
                    yield scrapy.Request(url=e, callback=self.crawl_gpsearch, meta=meta)
                else:
                    pass
            pass




        ########################################################################################
        # Site includes 'departments' element

        departments_xpath   = '//div[@id="departments"]'

        if bool(response.selector.xpath(departments_xpath)):
            
            log.msg('Found "department" element', level=log.INFO)
            

            #intend_1_xpath      = '//div[@id="departments"]//li[@class="a-spacing-micro s-navigation-indent-1"]/span/span/text()'
            intend_2_xpath      = '//div[@id="departments"]//li[@class="a-spacing-micro s-navigation-indent-2"]//a'

            #current_category = response.selector.xpath(intend_1_xpath).get()
            links = response.selector.xpath(intend_2_xpath)

            for e in links:
                url     = check_url(e.xpath('@href').get())
                name    = e.xpath('./span/text()').get()
                if not url in self.visited_websites:
                    self.visited_websites.add(url)
                    yield scrapy.Request(url=url, callback=self.main_crawl, meta=add_cat_to_meta(response.meta, [name]))
                else:
                    pass




    def crawl_gpsearch(self, response):
        """
        Crawler to crawl 'gp/search' pages, e.g. top brands, specific sellers etc.
        The crawler will folow the first results only. There might be further crawling
        to every category inside the /gp/search page
        """

        log.msg('Crawl "/gp/search/" on %s' % response.url, level=log.INFO)


        cat_type_xpath = '//span[@class="a-color-state"]/text()'
        cat_type = response.selector.xpath(cat_type_xpath).get()

        ## page without 'center' element

        jumbotron_box_xpath = '//div[@class="a-row a-spacing-none s-see-all-c3-refinement s-see-all-refinement-list"]'
    
        links = response.selector.xpath(jumbotron_box_xpath + '//a')
        for e in links:
            name = e.xpath('.//text()').get()
            url = check_url(e.xpath('@href').get())

            # pass category as a dict to define type of category
            if not url in self.visited_websites:
                self.visited_websites.add(url)
                yield scrapy.Request(url=url, callback=self.crawl_department, meta=add_cat_to_meta(response.meta, [{cat_type: name}]))
            else:
                pass




    def crawl_department(self, response):
        """
        Spider for crawling the department pages, which is the last 
        category. This spider collects the Product IDs.
        """        
        # get product IDs

        log.msg('Crawl "department" on %s' % response.url, level=log.INFO)

        product_reg = '.*(\/dp\/([A-Z0-9]{10})).*'
        dp_reg      = '.*(\\/dp\\/).*' 

        product_links = response.selector.xpath('//div[@class="s-result-list s-search-results sg-row"]//a')

        products_on_page_set = set() # multiple links may appear

        for e in product_links:
            url = e.xpath('@href').get()
            if re.match(dp_reg, url):
                products_on_page_set.add(re.search(product_reg, url).group(2))
        
        

        
        ##################################
        # call Product specific reviewpages
        for e in list(products_on_page_set):
            
            item = SpiderItem()
            item['product_id']  = str(e)
            item['category']    = response.meta['catgry']

            # category
            meta_loc = copy.deepcopy(response.meta)
            meta_loc['cat_info'] = item
            url_loc = str(self.amazon_domain + '/product-reviews/' + str(e))
            if not url_loc in self.visited_websites:
                self.visited_websites.add(url_loc)
                yield scrapy.Request(url=url_loc,callback=self.crawl_reviews, meta=meta_loc)
            else:
                pass

        ##################################


        # Page Navigation

        page_reg = r'.*(page=(\d+)).*'

        pagination_xpath = '//ul[@class="a-pagination"]//a'

        next_pages = response.selector.xpath(pagination_xpath)

        # if more than one page exists
        if bool(next_pages):
            
            
            page_list = dict()

            for e in next_pages:
                url = check_url(e.xpath('@href').get())
                
                if bool(re.match(page_reg, url)):
                    number = re.search(page_reg, url).group(2)
                    page_list[str(number)] = url
                else:
                    page_list[str(1)] = url



            # first page
            if not re.match(page_reg, response.url):
                if not page_list['2'] in self.visited_websites:
                    self.visited_websites.add(page_list['2'])
                    yield scrapy.Request(url=page_list['2'], callback=self.crawl_department, meta=response.meta)
                else:
                    pass
            
            else:

                try:
                    number = int(re.search(page_reg, response.url).group(2))
                except e:
                    self.logger.info("ERROR (CRAWL DEPARTMENT -> PAGE NAVIGATION) : %s", str(e))

                # if there exists a next page
                if str( (number + 1) ) in page_list:
                    url = page_list[str( (number + 1) )]
                    if not url in self.visited_websites:
                        yield scrapy.Request(url=url, callback=self.crawl_department, meta=response.meta)
                    else:
                        pass
                else:
                    output = "\nSpider closed. Crawling for %s finished!\n" % ( re.search('.*(amazon.com\/)(.*)(\/).*', response.url).group(2) )

                    log.msg(output, level=log.INFO)
                pass
        else:
            output = "\nSpider closed. Crawling for %s finished!\n" % ( re.search('.*(amazon.com\/)(.*)(\/).*', response.url).group(2) )
            log.msg(output, level=log.INFO)

        pass 

    


    def crawl_reviews(self, response):
        """
        Final crawl for the user reviews. Crawler will follow the next pages 
        until there is no more content. (No further review pages)
        """

        log.msg('Crawl reviews on %s' % response.url, level=log.INFO)

        ## page navigation
        nextpage_xpath      = '//li[@class="a-last"]//a'
        page_number_reg     = '(pageNumber=)(\d+)' 
        lastpage_xpath      = '//li[@class="a-disabled a-last"]'
        id_reg              = '(product-reviews\/)([A-Z0-9]*)'       

        ## user review paths
        review_div_xpath    = '//div[re:test(@id, "customer_review-(.*)")]'
        review_text_xpxath  = './/span[@class="a-size-base review-text review-text-content"]/span/text()'
        review_rating_xpath = './/i[re:test(@class, "a-icon a-icon-star (.*)")]/span/text()'
        helpful_vote_xpath  = './/span[@class="cr-vote"]/div[@class="a-row a-spacing-small"]/span/text()'
        meta_info_xpath     = './/span[re:test(@class, "(review-date)")]/text()'
        user_id_xpath       = './/span[@class="a-profile-name"]/text()'



        ## get user review info
        reviews_on_page = response.selector.xpath(review_div_xpath)

        for e in reviews_on_page:
            item = AmazonReviewItem()
            item['meta']            = response.meta['cat_info']
            item['text']            = e.xpath(review_text_xpxath).get()
            item['rating']          = e.xpath(review_rating_xpath).get()
            item['helpful_vote']    = e.xpath(helpful_vote_xpath).get()
            item['meta_info']       = e.xpath(meta_info_xpath).get()
            item['user_id']         = e.xpath(user_id_xpath).get()
            log.msg('Item scraped %s' % str(item['meta']['product_id']), level=log.INFO)
            yield item

        ## page navigation
        if bool(response.selector.xpath(nextpage_xpath).get()):
            page_link           = response.selector.xpath('//li[@class="a-last"]//a/@href').get()
            next_page_number    = re.search(page_number_reg, page_link).group(2)
            product_id          = re.search(id_reg, response.url).group(2)
            url                 = (self.amazon_domain + '/product-reviews/' + product_id + '?pageNumber=' + next_page_number)
            yield scrapy.Request(url=url, callback=self.crawl_reviews, meta=response.meta)
           
        if bool(response.selector.xpath(lastpage_xpath).get()):
            
            return




