# AmaScrape

Amazon User Review Scraper


## Scrape leftNav pages
 - on every leftNav page, the Scraper will only get the links from the first box 
    (`//ul[@class="a-unordered-list a-nostyle a-vertical s-ref-indent-two"]`)
    in further version a crawl through links in the "Refine By" Box may be included
 - Crawl as long there are elements in the box or there is a department page 
   - if there is no more list left (Department page or simply no more items), another crawling funciton is to be called


## Scrape specific Category page (gpsearch)
 - At this point, we are limiting the search for the top results only (although it is possible to go further to all entries (A-Z))
 